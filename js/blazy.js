/**
 * @file
 * Initializes Blazy.
 */

(function (Drupal) {

  'use strict';

  /**
   * Initializes Blazy.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.blazy_blurry_placeholder = {
    attach: function (context) {
      var bLazy = new Blazy();
    }
  };

}(Drupal));
